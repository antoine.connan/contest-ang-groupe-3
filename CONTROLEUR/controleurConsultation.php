<?php

require_once ('../MODELE/ConnexionModele.class.php');
require_once ('../MODELE/UtilisateursModele.class.php');

//permet à la VUE consultationEquipesCoureur de récupérer la liste des équipes avec leur association
//pas besoin d'AJAX ici, cette récupération est faite au chargement de la page
function getAllUser()
{
$UTIMod = new UtilisateursModele();
return $UTIMod->getAllUser(); //requete via le modele
}

function verifConnexion($id, $mdp){ //liste des coureurs par équipe
	//méthode avec un paramètre permettant de récupérer la liste des sponsors
		$CONMod = new ConnexionModele();
		return $CONMod->verifConnexion($id, $mdp);
	}

?>
