<?php
require_once ('../MODELE/UtilisateursModele.class.php');
$msgERREUR = "";
if (isset ( $_POST ['login'] ) && isset ( $_POST ['mdp'] )) {

	// ajout de l'équipe en récupérant l'Id de l'association (ici dans select_association)
	$modeleUTI = new UtilisateursModele ();
	try {

		$nb = $modeleUTI->add($_POST ['login'],$_POST ['mdp']);

		$msgERREUR = "SUCCESS :Inscription reussi !";

	} catch ( PDOException $pdoe ) {
		// cas ou 2 pseudo ont deja mis un commentaire sur un jeu
		$msgERREUR = "ERREUR : Erreur dans l'ajout de l'utilisateur ! : <br/>" . $pdoe->getMessage ();
	}
}
header ( 'Location: ../VUE/connexion.php?error='.$msgERREUR.'');
?>
