<?php
require_once ('../Class/autoload.php');
require_once ('../MODELE/CoureurModele.class.php');
$msgERREUR = "";

if (isset ($_GET['idDelete'])) {
	$modeleCOU = new CoureurModele();
	try {
			//requête permettant de supprimer une personne
			$nbCOU = $modeleCOU->delete($_GET['idDelete']);
			$msgERREUR = "SUCCESS : suppression du coureur";			
	} catch ( PDOException $pdoe ) {
		// erreur attrapée de MySQL
		$msgERREUR .= "ERREUR dans la suppression du coureur !  <br/>" . $pdoe->getMessage ();
	}
}
// redirection vers la page appelante en lui passant le message d'erreur
header('Location: ../VUE/consultationCoureurs.php?error='.$msgERREUR);
?>