<?php
class PageBase {
	private $style = array (
			'main'
	); // mettre juste le nom du fichier SANS l'extension css
	private $style_bootstrap = array (
			'animsition/animsition.min',
			'font-awesome-5/css/fontawesome-all.min',
			'bootstrap-4.1/bootstrap.min'
	);
	private $script = array (
			'jquery',
			'utile',
			'main' //script gérant les affichages des messages d'erreur et aussi la redirection
	); // mettre juste le nom du fichier SANS l'extension js
	private $script_bootstrap = array (
			'animsition/animsition.min',
			'bootstrap-4.1/bootstrap.min',
			'bootstrap-4.1/popper.min'
	);
	private $scriptExec = array(); //script que l'on veut executer
	private $motsCles;
	private $description;
	private $titre;
	private $entete;
	protected $menu;
	private $contenu;
	private $zoneErreur;
	private $piedpage;
	public function __construct($t) {
		$this->titre = $t;
		$this->description = 'DESCRIPTION_DU_SITE';
		$this->motsCles = 'MOTS_CLÉS_DU_SITE';
		$this->entete = '';

		$this->menu = '<nav class="navbar navbar-expand-lg navbar-dark fixed fixed-top">
  <a class="navbar-brand" href="index.php">CONTEST - Puissance 4</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    </ul>
    <form class="form-inline my-2 my-lg-0" style="color: #fff;">
			<p class="text-center" style="margin: auto;">Bienvenue - Connectez-vous ou Inscrivez-vous</p>
    </form>
  </div>
</nav>
<script>
		$(window).scroll(function() {
				var offset = $(window).scrollTop();
				console.log(offset);
				$(".navbar").toggleClass("trans", offset > 50);
		});
</script>';

		$this->zoneErreur='';

		$this->piedpage = '<footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h6>A propos</h6>
            <p class="text-justify">Ce site a été créé dans le but de renforcer la motivation et la créativité de nos employé. Il permet de jouer au puissance 4, ce jeu très connu ! Il permet également de jouer à deux sur le même ordianteur ou contre la machine et enregistre les scores.</p>
          </div>

          <div class="col-xs-6 col-md-3">
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Liens</h6>
            <ul class="footer-links">
              <li><a href="https://gitlab.com/antoine.connan/contest-ang-groupe-3">Le projet</a></li>
              <li><a href="https://campus.academy/campus/angers">Campus academy</a></li>
            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Copyright &copy; 2020 Tous droits réservés
         <a href="https://gitlab.com/antoine.connan/contest-ang-groupe-3">Antoine CONNAN & Lucas MERCIER</a>.
            </p>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="twitter" href="https://gitlab.com/antoine.connan">AC</a></li>
              <li><a class="twitter" href="https://gitlab.com/lucasmercier">LM</a></li>
            </ul>
          </div>
        </div>
      </div>
</footer>';
	}
	public function __set($propriete, $valeur) {
		switch ($propriete) {
			case 'motsCles' :
				{
					$this->motsCles .= $valeur;
					break;
				}
			case 'style' :
				{
					$this->style [count ( $this->style )] = $valeur;
					break;
				}
			case 'script' :
				{
					$this->script [count ( $this->script )] = $valeur;
					break;
				}
			case 'scriptExec' :
				{
					$this->scriptExec [count ( $this->scriptExec )] = $valeur;
					break;
				}

			case 'titre' :
				{
					$this->titre = $valeur;
					break;
				}
			case 'contenu' :
				{
					$this->contenu = $valeur;
					break;
				}
		}
	}
	public function __get($propriete) {
		switch ($propriete) {

			case 'contenu' :
				{
					return $this->contenu;
					break;
				}
		}
	}
	/**
	 * ***********Gestion des mots clés *************************
	 */
	/* Insertion des mots clés */
	private function charge_motsCles() {
		echo "<meta name='keywords' lang='fr' content='" . $this->motsCles . "' />";
		echo ("\n");
	}

	/**
	 * ********Gestion de la description ***********************
	 */
	/* Insertion de la description du site */
	private function charge_description() {
		echo "<meta name='description' content='" . $this->description . "'/>";
		echo ("\n");
	}
	/**
	 * ***********Gestion des styles **********************
	 */
	/* Insertion des feuilles de style */
	private function charge_style() {
		foreach ( $this->style as $s ) {
			echo "<link rel='stylesheet' type='text/css' href='../VUE/css/" . $s . ".css'/>";
			echo ("\n");
		}
	}
	private function charge_style_bootstrap() {
		foreach ( $this->style_bootstrap as $s ) {
			echo "<link rel='stylesheet' type='text/css' href='../VUE/vendor/" . $s . ".css'/>";
			echo ("\n");
		}
	}

	/**
	 * ********Gestion des scripts ***********************
	 */
	/* Insertion des script JAVASCRIPT */
	private function charge_script() {
		foreach ( $this->script as $sc ) {
			echo "<script type='text/javascript' src='../VUE/js/" . $sc . ".js'></script>\n";
		}
	}
	private function charge_script_bootstrap() {
		foreach ( $this->script_bootstrap as $sc ) {
			echo "<script src='../VUE/vendor/" . $sc . ".js'></script>\n";
		}
	}
	/**
	 * ****************Gestion des scripts A EXECUTER *******************
	 */
	/* EXECUTION des script JAVASCRIPT */
	private function exec_script() {
		foreach ( $this->scriptExec as $se ) {
			echo "<script type='text/javascript'>". $se."</script>";
		}
	}
	/**
	 * ***********Gestion de l'entete **********************
	 */
	protected function affiche_entete() {
		echo $this->entete;
	}
	/**
	 * **********Gestion du pied de page ***********************
	 */
	private function affiche_pied_page() {
		echo $this->piedpage;
	}
	/**
	 * **********Gestion du menu ************************
	 */
	protected function affiche_menu() {
		echo $this->menu;
	}
	/**
	 * ********METHODE AFFICHER ***********************
	 */
	public function afficher() {
		?>
<!DOCTYPE html>
<html lang='fr'>
<head>
<title> <?php echo $this->titre;?> </title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
	<?php $this->charge_motsCles();?>
	<?php $this->charge_description();?>
	<?php $this->charge_style_bootstrap();?>
	<?php $this->charge_style();?>
	<?php $this->charge_script_bootstrap();?>
	<?php $this->charge_script();?>
</head>
<body>
		<div class="container-fluid"><?php $this->affiche_entete();?></div>
		<div class="container-fluid" style="padding: 0 0 0 0;"><?php $this->affiche_menu();?></div>
		<div class="container-fluid" style="margin-top: 100px;"><?php echo $this->contenu;?></div>
		<?php $this->affiche_pied_page();?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src='https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js'></script>
</body>
</html>
<?php
	}
}
?>
