<?php
class PageSecurisee extends PageBase {
	public function __construct($t) {
		parent::__construct($t);
	}
	/**
	 ************ Gestion du menu ****************
	 */
	// REDEFINITON du menu par rapport à celui de page_base
	protected function affiche_menu() {

		// on rajoute dans le MENU
		// le menu Déconnexion : possiblité de se deconnecter du mode administrateur
		// 2 nouvelles pages "inscription coureurs et Association Caritative !
		$this->menu ='<nav class="navbar navbar-expand-lg navbar-dark fixed fixed-top">
  <a class="navbar-brand" href="index.php">CONTEST - Puissance 4</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Jouer<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Scores</a>
      </li>
			<li class="nav-item">
        <a class="nav-link" href="#">A propos</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0" style="margin-right: 5%;">
			<div class="dropdown" style="margin-top: -50%;">
				<button class="btn btn-secondary dropdown-toggle" style="background-color: rgba(255,255,255,0); border: none; margin-top: 40px;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<b>'.$_SESSION['IDENTIFIANT'].'</b>
				</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					<a class="dropdown-item" href="../fonctions/deconnexion.php">Deconnexion</a>
				</div>
			</div>
    </form>
  </div>
</nav>
<script>
		$(window).scroll(function() {
				var offset = $(window).scrollTop();
				console.log(offset);
				$(".navbar").toggleClass("trans", offset > 50);
		});
</script>';

		echo $this->menu;
	}
}
?>
