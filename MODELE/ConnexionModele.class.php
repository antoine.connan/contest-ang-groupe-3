<?php
require_once '../Class/Connexion.class.php';

class ConnexionModele {

	private $idCON = null;

	public function __construct() {
		try {
			$Connexion = new Connexion();
			$this->idCON = $Connexion->IDconnexion;
		} catch ( PDOException $e ) {
			echo "<h1>probleme access BDD</h1>";
		}
	}
	public function verifConnexion($identifiant,$motDePasse){

		$nb = 0;

		if ($this->idCON) {

			$prep = $this->idCON->prepare("SELECT count(*) as CPTCON from utilisateurs i  WHERE login = :id AND mdp = :mdp");
            $prep->bindParam(":id",$identifiant);
            $prep->bindParam(":mdp",md5($motDePasse));

		  $prep->execute();// si nb ==1 alors l'insertion s est bien passee
			$nb = $prep->fetch(PDO::FETCH_OBJ)->CPTCON;

		return $nb; // si nb =1 alors l'insertion s est bien passee
		}
	}

	public function getIdETU($identifiant,$motDePasse){

		$id = "";

		if ($this->idCON) {
			$prep = $this->idCON->prepare("SELECT id_utilisateur as SLCTID from utilisateurs i  WHERE login = :id AND mdp = :mdp");
						$prep->bindParam(":id",$identifiant);
						$prep->bindParam(":mdp",md5($motDePasse));

			$prep->execute();// si nb ==1 alors l'insertion s est bien passee
			$id = $prep->fetch(PDO::FETCH_OBJ)->SLCTID;

		return $id;
		}
	}
	public function getMail($identifiant,$motDePasse){

		$mail = "";

		if ($this->idCON) {
			$prep = $this->idCON->prepare("SELECT email as SLCTMAIL from utilisateurs i  WHERE login = :id AND mdp = :mdp");
						$prep->bindParam(":id",$identifiant);
						$prep->bindParam(":mdp",md5($motDePasse));

			$prep->execute();// si nb ==1 alors l'insertion s est bien passee
			$mail = $prep->fetch(PDO::FETCH_OBJ)->SLCTMAIL;

		return $mail;
		}
	}
}
