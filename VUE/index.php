<?php
session_start ();

require_once ('../Class/PageBase.class.php');
require_once ('../Class/PageSecurisee.class.php');
require_once ('../CONTROLEUR/controleurConsultation.php');

if (!isset($_SESSION['IDENTIFIANT']) && !isset($_SESSION['MOTDEPASSE'])) {
  header ('Location: connexion.php' );
  die();
}

$pageIndex = new PageSecurisee( "CONTEST - Puissance 4" );

$AllUsers = getAllUser();

$pageIndex->contenu = '
<img src="https://lh3.googleusercontent.com/proxy/77fvZ3i0bWwcTw7E3VhFZ7weP7XEiBX2Hbdi9W3Au0GnyW91IiM5qKf8u9LvcmsoISmidEmH95kpecUqWxP-J-iq5K7po-yhwh54JytKTD234Q6PkgXV4d2Rq4zBanVBBWNe" style="position:absolute;height:70px;width:70px;left:250px;top:50px">
<div class="row">
	<div class="col-md-8" style="margin: auto;">
		<div class="card" style="background-color: #eee; box-shadow: 1px 1px 5px 5px rgba(0,0,0,0.125); margin-top: 20px;">
			<div class="card-body" style="margin: auto;">
				<h5 class="card-title" style="text-align: center;"><b>Bienvenue !</b></h5>
				<p class="card-text">Afin d\'améliorer la motivation, la productivité ainsi que la crétivité de nos employé, nous avons mit en place un Puissance 4 !</p>

			</div>
		</div>
	</div>
</div>

<br>
<br>

<div class="row">
	<div class="col-md-8" style="margin: auto;">
		<div class="card" style="background-color: #eee; box-shadow: 1px 1px 5px 5px rgba(0,0,0,0.125); height: 600px;">
			<div class="card-body" style="margin: auto;">
				<h5 class="card-title">Jouez au Puissance 4 !</h5>
        <div class="row">
          <div class="col-md-12" id="game">
          </div>
          <script src="js/main.js"></script>
        </div>
      </div>
    </div>
  </div>
</div>';

$pageIndex->afficher ();
?>
