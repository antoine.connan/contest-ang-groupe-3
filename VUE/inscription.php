<?php
session_start ();
require_once ('../Class/PageBase.class.php');
require_once ('../Class/PageSecurisee.class.php');
require_once ('../CONTROLEUR/controleurConsultation.php');

$pageConnexion = new PageBase ( "CONTEST - Puissance 4" );

$pageConnexion->contenu = '<style>#v00 {display: none;}</style>
<div class="main-content" style="min-height: 0vh;">
<div class="section__content section__content--p30">
		<div class="container-fluid">';

if (isset($_GET['error']) && !empty($_GET['error'])) {
	$verif = preg_match("/ERREUR/",$_GET['error']); //verifie s'il y a le mot erreur dans le message retourné
	if ( $verif == FALSE ){
		$pageConnexion->contenu .= '
												<div class="row">
													<div class="col-md-3" style="margin: auto;">
														<div class="alert alert-success" role="alert"><a href="#" onclick="cacher();" class="close" data-dismiss="alert">&times;</a>
															<h4 class="alert-heading">Déconnexion réussi !</h4>
															<p>'.$_GET['error'].'</p>
														</div>
													</div>
												</div>';
	}else{
		$pageConnexion->contenu .= '<div class="row">
			<div class="col-md-3" style="margin: auto; margin-top: 6%;">
		<div class="alert alert-danger" role="alert"><a href="#" onclick="cacher();" class="close" data-dismiss="alert">&times;</a>
															<h4 class="alert-heading">Echec de connexion !</h4>
															<p>'.$_GET['error'].'</p>
														</div>
														</div>
													</div>';
	}
}
$pageConnexion->contenu .= '
</div>
</div>
</div>';

		$pageConnexion->contenu .= '
		<div class="row">
			<div class="col-md-3" style="margin: auto;">
				<div class="card" style="background-color: #eee; box-shadow: 1px 1px 5px 5px rgba(0,0,0,0.125);">
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<h2>Inscription</h2>
							</div>
							<div class="col-md-12">
								<div class="card-body">
									<form  id="formInscriptionAdmin" method="POST" action="../CONTROLEUR/tt_AjoutUtilisateur.php">
										<div class="form-group">
											<label for="adresse"><b>Identifiant</b></label>
											<input type="text" class="form-control" name="login" id="IDENTIFIANT" size="15" maxlength="15" placeholder="Identifiant" autofocus required >
										</div>
										<div class="form-group">
											<label for="adresse"><b>Mot de passe</b></label>
											<input type="password" class="form-control" name="mdp" id="mdp1" size="15" maxlength="15" placeholder="Mot de passe" required>
										</div>
										<div class="form-group">
											<label for="adresse"><b>Confirmer le mot de passe</b></label>
											<input type="password" class="form-control" name="mdp2" id="mdp2" size="15" maxlength="15" placeholder="Mot de passe" required onchange="verifMDP();">
										</div>
										<button type="submit" class="btn btn-primary">Valider</button>
									</form>
								</div>
								<div class="card-footer">
								Retourner à la <a href="connexion.php" style="font-weight: bold;">connexion</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>
			function verifMDP() {
				var mdp1 = document.getElementById("mdp1").value;
				var mdp2 = document.getElementById("mdp2").value;

				if (mdp1 != mdp2) {
					mdp1 = "";
					mdp2 = "";
					alert("Les mots de passe sont différents !");
					return false
				}else {
					return true
				}
			}
		</script>
	';

$pageConnexion->afficher();
