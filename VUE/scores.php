<?php
session_start ();

require_once ('../Class/PageBase.class.php');
require_once ('../Class/PageSecurisee.class.php');
require_once ('../CONTROLEUR/controleurConsultation.php');

if (!isset($_SESSION['IDENTIFIANT']) && !isset($_SESSION['MOTDEPASSE'])) {
  header ('Location: connexion.php' );
  die();
}

$pageIndex = new PageSecurisee( "CONTEST - Puissance 4" );


$pageIndex->contenu = '<div class="row">
	<div class="col-md-8" style="margin: auto;">
		<div class="card" style="background-color: #eee; box-shadow: 1px 1px 5px 5px rgba(0,0,0,0.125);">
			<div class="card-body" style="margin: auto;">
				<h5 class="card-title">Jouez au Puissance 4 !</h5>
        <div id="game">
          Veuillez activer javascript pour jouer.
        </div>
			</div>
		</div>
	</div>
</div>';

$pageIndex->afficher ();
?>
