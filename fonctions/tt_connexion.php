<?php
session_start ();
include ('../Class/autoload.php');
require_once('../CONTROLEUR/controleurConsultation.php');

	// traitement du formulaire (si on vient du formulaire alors
	if (isset($_POST['IDENTIFIANT']) && isset($_POST['MOTDEPASSE'])) {

		$id = $_POST['IDENTIFIANT'];
		$mdp = $_POST['MOTDEPASSE'];

		$nb = verifConnexion($id,$mdp);

		// Session active : on va verifier si les identifiants de connexion sont valides (ici login et motDepasse en dur dans le programme)
		// mais on pourrait récupérer le login et mot de passe de la BDD
		if ($nb == 1) {

			$_SESSION['IDENTIFIANT'] = $id;
			$_SESSION['MOTDEPASSE'] = $mdp;

			// on appelle la nouvelle classe Page_sécurisée :  page avec un menu specifique
			$page = new PageSecurisee ("Admin TerredePixels - Connecté");

      if (isset($_SESSION['IDENTIFIANT']) && isset($_SESSION['MOTDEPASSE'])) {
        header ('Location: ../VUE/index.php' );
      }
		}else {
			// les identifiants de connexion existe mais ne sont pas VALABLES
			//header ('Location:../VUE/connexion.php?error=ERREUR : Login ou mot de passe non valide !');
		}
	}
